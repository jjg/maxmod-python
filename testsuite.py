# testsuite for maxmod.py
# J.J. Green 2010, 2015

import sys
import numpy as np
import maxmod


def mmverify(p, answer, relerr):
    """
    Verify that `p` has maxmod of `answer` to the requested realtive
    accuracy `relerr`.
    """

    R = maxmod.maxmod(p, epsilon=relerr, verbose=False, detailed=True)

    if R is None:
        return None

    arelerr = abs(R['M'] - answer) / min(R['M'], answer)

    sys.stdout.write(
        "{0:3d} {1:6.4e} {2:5d}\t".format(len(p), relerr, R['evals'])
    )

    if arelerr > relerr:
        print("failed\n")
        print("required relative error < {0:e}".format(elerr))
        print("actual relative error = {0:e}".format(aelerr))
        return 0
    else:
        print("passed")
        return 1


print("maxmod python testsuite")

p = 0
n = 0

# check that maxmod() is getting the right answers to
# the right relative accuracy
#
# note: we use numpy arrays for the mmverify just so
# they are a bit easier to manipulate, but one can just
# use plain python lists if you like

# simple case

a = np.array([1, 1, 1])
p += mmverify(a, 3, 1e-3)
p += mmverify(a, 3, 1e-6)
p += mmverify(a, 3, 1e-9)
n += 3

# slightly bigger

a = np.array([1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1])
p += mmverify(a, 2, 1e-3)
p += mmverify(a, 2, 1e-6)
p += mmverify(a, 2, 1e-9)
p += mmverify(a, 2, 1e-12)
n += 4

# large values

p += mmverify(1e12 * a, 2e12, 1e-12)
n += 1

# complex values

p += mmverify(1j * a, 2, 1e-12)
n += 1

# time Consuming

a = np.zeros((300))
a[0] = 30
a[-1] = 1

p += mmverify(a, 31, 1e-3)
p += mmverify(a, 31, 1e-6)
p += mmverify(a, 31, 1e-9)
p += mmverify(a, 31, 1e-12)
n += 4

# regression for beta0 bug, this will use up all of
# your memory if the bug is present !

a = np.array([1, 0, 0])
p += mmverify(a, 1, 1e-12)
n += 1

print("Total number of tests: {0}".format(n))
print("Passed: {0}".format(p))
print("Failed: {0}".format(n - p))

if n == p:
    status = 0
else:
    status = 1

sys.exit(status)
