# The maxmod algorithm, python3 version
#
# J.J. Green, 2010, 2012, 2015
#
# This code is contributed to the public domain.
#
# The author requests that derived code is named in such a way
# as to avoid confusion with the original version.

# mypy: disable-error-code="unused-ignore"

from __future__ import annotations

import sys
import numpy as np
from typing import Any, Union, TypedDict
from numpy.typing import ArrayLike, NDArray


class MaxmodDetails(TypedDict):
    '''
    Return type when maxmod is called with detailed True
    '''
    M: float
    h: float
    t: NDArray[np.double]
    evals: int


def maxmod(
        p: ArrayLike,
        epsilon: float = 1e-8,
        verbose: bool = False,
        detailed: bool = False,
        initial: int = -1
) -> Union[MaxmodDetails, float]:
    """
    The maximum modulus of a complex polynomial p on the unit
    disc is estimated using a method of repeated subdivision
    of [0, 2pi] and rejection of non-maximizing subintervals.
    The rejection criterion is based on a lemma of S.B. Steckin.
    The algorithm is described in

      J.J. Green. Calculating the maximimum modulus of a polynomial
      with Steckin's lemma. SIAM J. Numer. Anal., 83:211-222, 2000.

    The arguments are
    p         - a list of complex values, the coefficients of the
                polynomial

    epsilon   - the relative accuracy required

    verbose   - a boolean, if true print debugging/user information

    detailed  - a boolean, if true return a dictionary containing
                - M     : the maximum modulus
                - t     : and array of maximising interval midpoints
                - h     : the interval half-width
                - evals : the number of polynomial evaluations used
                otherwise the return value is just the maximum modulus

    initial   - the initial number of intervals used in the algorithm:
                at least twice the length of p
    """

    # printf style info function

    if verbose:
        def info(fmt: str, *args: Any) -> None:
            """
            Print information to stdout
            """
            sys.stdout.write(fmt % args)
    else:
        def info(fmt: str, *args: Any) -> None:
            """
            No-op method for silent operation
            """
            pass

    info("This is maxmod\n")

    # elementwise |x|^2 (actually returns NDArray[np.double]),
    # but my current system numpy real/imag returns Any ...

    def abs2(z: NDArray[np.cdouble]) -> Any:
        return np.real(z)**2 + np.imag(z)**2

    # convert p to a numpy array

    p = np.array(p)

    # size of list

    n: int = np.size(p)

    # negative initial used to indicate default value (we can't
    # use the Python default mechanism directly since we don't
    # know what n is until we've inspected p)

    if initial < 0:
        initial = 5 * n

    # check feasibility of initial arg

    if initial < 2 * n:
        msg: str = "initial intervals must be at least {0}".format(2 * n)
        raise ValueError(msg)

    beta: NDArray[np.cdouble] = np.convolve(p[::-1], np.conj(p))
    beta0: float = np.real(beta[n - 1])
    normq: float = sum(abs(beta))

    evals: int = 0

    # check if close enough to a monomial

    m2_min: float
    m2_max: float

    if normq - beta0 < 4 * epsilon * beta0:

        m2_min = beta0
        m2_max = normq

        m: float = np.sqrt((m2_min + m2_max) / 2)

        if detailed:
            return {
                'M': m,
                't': np.array([]),
                'h': 2 * np.pi,
                'evals': 0
            }
        else:
            return m

    m2_min = 0
    m2_max = np.inf

    a: float = max([2 * beta0 - normq, 0])

    # v, the interval-set is a 2d-array
    #
    # [z0 q0
    #  z1 q1
    #    :
    #  zn qn]
    #
    # where qi = |p(zi)|^2

    h: float = np.pi / initial

    v: NDArray[np.double] = np.zeros((initial, 2))
    v[:, 0] = (2 * h) * np.arange(0, initial)
    v[:, 1] = abs2(np.fft.fft(p, initial))

    eval0: int = initial
    evals = initial

    info('lower bound\tupper bound\tevals\trejected\n')

    while True:

        # evaluate bounds

        m2_min = np.max(v[:, 1])  # type: ignore
        sc: float = np.cos(n * h)
        m2_max = (m2_min - a) / sc + a

        if m2_max - m2_min < 4 * epsilon * m2_min:
            info(
                "{0}\t{1}\t{2}\t-\n".format(
                    np.sqrt(m2_min),
                    np.sqrt(m2_max),
                    eval0
                )
            )
            break

        # rejection

        rt: float = (m2_min - a) * sc + a
        ind: NDArray[np.int_] = np.nonzero(v[:, 1] > rt)[0]
        nrej: int = np.size(v, 0) - ind.shape[0]
        v = np.array(v[ind, :])

        info(
            "{0}\t{1}\t{2}\t{3}\n".format(
                np.sqrt(m2_min),
                np.sqrt(m2_max),
                eval0,
                nrej
            )
        )

        # subdivision

        h = h / 3
        offset: float = 2 * h

        # new t, z values

        t: NDArray[np.double] = v[:, 0]
        t = np.concatenate((t + offset, t - offset))

        z: NDArray[np.cdouble] = np.exp(t * 1j)

        # update stats

        eval0 = t.shape[0]
        evals += eval0

        # q = |p(z)|**2 calculated using vectorized Horner's rule

        qz: NDArray[np.double] = abs2(np.polyval(p, z))

        # assign new values

        v = np.vstack((v, np.vstack((t, qz)).T))

    # we only get here from the break

    m = np.sqrt((m2_min + m2_max) / 2)

    if detailed:
        return {
            'M': m,
            't': np.sort(v[:, 0]),
            'h': h,
            'evals': evals
        }
    else:
        return m
